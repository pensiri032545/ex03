void main() {
  Person person = new Person();

  person.name = {'name': 'Andrea'};
  person.age = {'age': 36};
  person.height = {'height': 1.84};

  person.printDescription();
}

class Person {
  late Map _name;
  late Map _age;
  late Map _height;

  set name(Map name) => this._name = name;
  set age(Map age) => this._age = age;
  set height(Map height) => this._height = height;

  Map get name => this._name;
  Map get age => this._age;
  Map get height => this._height;

  void printDescription() {
    print("My name is ${_name['name']}. I'm ${_age['age']} years old, I'm ${_height['height']} meters tall.");
  }
}
// 6450110008